<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\Random;

abstract class SpaceShip
{
    public $math;
    public $random;
    protected static bool $destroyed = false;
    public function __construct(
        public string $name,
        private int $strength,
        private int $armor,
        private int $luck,
        private int $health,
        private string $hold = '[ _ _ _ ]'
    ) {
        $this->math = new Math();
        $this->random = new Random();
    }

    public function getName()
    {
        return $this->name;
    }

    public function attack(SpaceShip $enemySpaceShip): int
    {
        $luck = $this->math->luck($this->random, $this->getLuck());

        $damage = $luck
            ? $this->math->damage($this->getStrength(), $enemySpaceShip->getArmor())
            : 0;

        return $damage;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getArmor(): int
    {
        return $this->armor;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getHold(): string
    {
        return $this->hold;
    }

    public function setHealth($health): void
    {
        if ($health > 100) {
            $health = 100;
        }
        $this->health = $health > 0 && $health <= 100 ? $health : 0;
    }


    public function setArmor(int $armor)
    {
        print 'Set armor';
        $this->armor = $armor;
    }

    public function setStrength($strength)
    {
        $this->strength = $strength;
    }

    public function setHold(string $hold)
    {
        $this->hold = $hold;
    }

    public static function setDestroyed()
    {
        static::$destroyed = true;
    }

    public static function getDestroyed()
    {
        return static::$destroyed;
    }

    public function holdIsEmpty(): bool
    {
        return false;
    }

    public function hasCrystal()
    {
        
    }

    public function setCrystal()
    {
        
    }

    public function decreaseHold()
    {

    }

    public function reactor()
    {
        
    }
}
