<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Tribute;
use BinaryStudioAcademyTests\Game\Messages;

class HomeSpaceShip extends SpaceShip
{
    protected static bool $destroyed = false;

    public function attack(SpaceShip $enemySpaceShip): int
    {
        $luck = $this->math->luck($this->random, $this->getLuck());

        $damage = $luck
            ? $this->math->damage($this->getStrength(), $enemySpaceShip->getArmor())
            : 0;

        return $damage;
    }

    public function holdIsEmpty(): bool
    {
        return substr_count($this->getHold(), '_') === 3 ? true : false;
    }

    private function countElement(string $element)
    {
        return substr_count($this->getHold(), $element);
    }

    public function grab(string $hold, string $shipName)
    {
        $lenCrystal = $this->countElement(Tribute::CRYSTAL);
        $lenReactor = $this->countElement(Tribute::REACTOR);

        $freeSpace = $lenCrystal + $lenReactor;
        if ($freeSpace === 3) {
            print Messages::errors('hold_is_not_empty');
            return;
        }
        $t = [];
        $arr = explode(' ', $hold);
        $inventar = explode(' ', $this->getHold());
        foreach ($arr as $value) {
            if ($value === Tribute::CRYSTAL) {
                $t[] = Tribute::CRYSTAL;
            } elseif ($value === Tribute::REACTOR) {
                $t[] = Tribute::REACTOR;
            }
        }
        $lent = count($t);
        $i = 0;
        for ($count = 0; $count < count($inventar); $count++) {
            if ($inventar[$count] === '_') {
                $i++;
                $inventar[$count] = array_pop($t);
            }

            if ($i === $lent) {
                break;
            }
        }

        if ($shipName === 'Patrol Spaceship') {
            print Messages::grabPatrolSpaceship();
        }

        if ($shipName === 'Battle Spaceship') {
            print Messages::grabBattleSpaceship();
        }

        $inventar = implode(' ', $inventar);
        // print $inventar;
        $this->setHold($inventar);
    }

    public function hasCrystal()
    {
        $crystal = $this->countElement(Tribute::CRYSTAL);

        return $crystal > 0 ? true : false;
    }

    public function setCrystal()
    {
        $inventar = explode(' ', $this->getHold());

        foreach ($inventar as $key => $item) {
            if ($item === Tribute::CRYSTAL) {
                $inventar[$key] = Tribute::REACTOR;
                break;
            }
        }

        $inventar = implode(' ', $inventar);
        $this->setHold($inventar);
    }

    public function decreaseHold()
    {
        $inventar = explode(' ', $this->getHold());

        foreach ($inventar as $key => $item) {
            if ($item === Tribute::CRYSTAL) {
                $inventar[$key] = '_';
                break;
            }
        }

        $inventar = implode(' ', $inventar);
        $this->setHold($inventar);
    }

    public function reactor()
    {
        if ($this->countElement(Tribute::REACTOR) === 0) {
            print 'Buy or Get Reactor';
            return;
        }
        $inventar = explode(' ', $this->getHold());

        foreach ($inventar as $key => $item) {
            if ($item === Tribute::REACTOR) {
                $inventar[$key] = '_';
                break;
            }
        }

        $inventar = implode(' ', $inventar);
        $this->setHold($inventar);

        $this->setHealth($this->getHealth() + 20);
        print Messages::applyReactor($this->getHealth());
    }
}
