<?php

namespace BinaryStudioAcademy\Game\Helpers;

class Singleton
{
    public static $instance;

    public static function getInstance($instance)
    {
        if (!isset(self::$instance)) {
            self::$instance = $instance;
        }
        return self::$instance;
    }
}
