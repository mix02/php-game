<?php

namespace BinaryStudioAcademy\Game\Galaxy;

use BinaryStudioAcademy\Game\Ships\BattleSpaceShip;
use BinaryStudioAcademy\Game\Ships\SpaceShip;

class Shiar extends Galaxy
{
    public function spaceFleet(): SpaceShip
    {
        return new BattleSpaceShip('Battle Spaceship', 8, 6, 3, 100, '[ 🔋 🔮 _ ]');
    }
}
