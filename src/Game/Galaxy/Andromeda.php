<?php

namespace BinaryStudioAcademy\Game\Galaxy;

use BinaryStudioAcademy\Game\Ships\PatrolSpaceShip;
use BinaryStudioAcademy\Game\Ships\SpaceShip;

class Andromeda extends Galaxy
{

    public function spaceFleet(): SpaceShip
    {
        return new PatrolSpaceShip('Patrol Spaceship', 4, 4, 2, 100, '[ 🔋 _ _ ]');
    }
}