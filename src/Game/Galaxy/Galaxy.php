<?php

namespace BinaryStudioAcademy\Game\Galaxy;

use BinaryStudioAcademy\Game\Ships\SpaceShip;

abstract class Galaxy
{
    public static SpaceShip | null $ship;

    public function __construct(public string $title)
    {
        if (isset(static::$ship)) {
            static::$ship = null;
        }

    }

    abstract public function spaceFleet(): SpaceShip;

    public static function createOnlyOneShip(SpaceShip $ship)
    {
        if (!isset(static::$ship)) {
            static::$ship = $ship;
        }

        return static::$ship;
    }

    public function buy(SpaceShip $spaceShip, string $element)
    {
        return "Enemy Galaxy";
    }
}
