<?php

namespace BinaryStudioAcademy\Game\Galaxy;

use BinaryStudioAcademy\Game\Ships\ExecutorSpaceShip;
use BinaryStudioAcademy\Game\Ships\SpaceShip;

class Isop extends Galaxy
{
    public function spaceFleet(): SpaceShip
    {
        return new ExecutorSpaceShip('Executor', 10, 10, 10, 100, '[ 🔋 🔮 🔮 ]');
    }
}
