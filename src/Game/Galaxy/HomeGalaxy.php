<?php

namespace BinaryStudioAcademy\Game\Galaxy;

use BinaryStudioAcademy\Game\Ships\SpaceShip;
use BinaryStudioAcademy\Game\Ships\HomeSpaceShip;
use BinaryStudioAcademyTests\Game\Messages;

class HomeGalaxy extends Galaxy
{
    public function spaceFleet(): SpaceShip
    {
        return new HomeSpaceShip('Traktor', 5, 5, 5, 100);
    }

    public function buy(SpaceShip $spaceShip, string $element)
    {
        // print 'Buy workd well!';
        if ($spaceShip->holdIsEmpty()) {
            print 'You have not enough money to buy items. Please earn money.';
            return;
        }

        $element = trim($element);
        if ($spaceShip->hasCrystal()) {
            switch ($element) {
                case 'strength':
                    $spaceShip->setStrength(10);
                    print Messages::buySkill('strength', $spaceShip->getStrength());
                    break;
                case 'armour':
                    $spaceShip->setArmor(10);
                    print Messages::buySkill('armor', $spaceShip->getArmor());
                    break;
                case 'reactor':
                    $spaceShip->setCrystal();
                    print Messages::buyReactor(20);
                    break;
            }

            $spaceShip->decreaseHold();
        }
    }

}
