<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Galaxy\Andromeda;
use BinaryStudioAcademy\Game\Galaxy\Galaxy;
use BinaryStudioAcademy\Game\Galaxy\HomeGalaxy;
use BinaryStudioAcademy\Game\Galaxy\Isop;
use BinaryStudioAcademy\Game\Galaxy\Pegasus;
use BinaryStudioAcademy\Game\Galaxy\Shiar;
use BinaryStudioAcademy\Game\Galaxy\Spiral;
use BinaryStudioAcademy\Game\Galaxy\Xeno;
use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\HomeSpaceShip;
use BinaryStudioAcademy\Game\Ships\SpaceShip;
use BinaryStudioAcademy\Game\Messages;
use BinaryStudioAcademy\Game\Ships\ExecutorSpaceShip;

class Game
{
    private $random;
    private Writer $writer;
    private Galaxy $galaxy;
    private HomeSpaceShip $homeSpaceShip;
    private SpaceShip|null $enemySpaceShip = null;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->writer = new CliWriter();
        $this->galaxy = new HomeGalaxy('home');
        $this->homeSpaceShip = new HomeSpaceShip('Traktor', 10, 10, 10, 100);
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln(Messages::help());

        $this->run($reader, $writer);
    }

    public function run(Reader $reader, Writer $writer)
    {
        while (true) {
            $input = trim($reader->read());

            if ($input === 'exit') {
                break;
            } elseif ($input === 'help') {
                $writer->writeln(Messages::help());
            } elseif (str_contains($input, 'set-galaxy')) {
                $this->selectGalaxy($input);
            } elseif ($input === 'attack') {
                $this->attack();
            } elseif (str_contains($input, 'buy')) {
                $this->buy($input, $this->galaxy->title, $this->homeSpaceShip);
            } elseif ($input === 'grab') {
                $this->grab($this->galaxy->title);
            } elseif ($input === 'whereami') {
                $this->whereami($this->galaxy->title);
            } elseif ($input === 'restart') {
                $writer->writeln('Restart');
                $this->restart();
            } elseif ($input === 'apply-reactor') {
                $this->applyReactor();
            } elseif ($input === 'stats') {
                $this->stats();
            }
        }
    }

    public function applyReactor()
    {
        $this->homeSpaceShip->reactor();
    }

    public function whereami(string $galaxyLabel): void
    {
        $this->writer->writeln(Messages::whereAmI($galaxyLabel));
    }

    public function attack(): void
    {
        if ($this->galaxy->title === 'home') {
            $this->writer->writeln(Messages::errors('home_galaxy_attack'));
            return;
        }
        $getEnemySpaceShip = $this->galaxy->spaceFleet();
        $this->enemySpaceShip = Galaxy::createOnlyOneShip($getEnemySpaceShip);
        $this->fight();
    }

    public function selectGalaxy(string $input): void
    {
        $galaxy = explode(' ', $input)[1];

        switch ($galaxy) {
            case 'home':
                $this->galaxy = new HomeGalaxy('home');
                break;
            case 'andromeda':
                $this->galaxy = new Andromeda('andromeda');
                break;
            case 'pegasus':
                $this->galaxy = new Pegasus('pegasus');
                break;
            case 'spiral':
                $this->galaxy = new Spiral('spiral');
                break;
            case 'shiar':
                $this->galaxy = new Shiar('shiar');
                break;
            case 'xeno':
                $this->galaxy = new Xeno('xeno');
                break;
            case 'isop':
                $this->galaxy = new Isop('isop');
                break;
            default:
                $this->writer->writeln(Messages::errors('undefined_galaxy'));
                break;
        }
    }

    public function fight()
    {
        $classname = get_class($this->enemySpaceShip);

        if ($classname::getDestroyed()) {
            $this->writer->writeln(Messages::crashShip($this->enemySpaceShip->name));
            return;
        }
        $humanDamage = $this->homeSpaceShip->attack($this->enemySpaceShip);
        $alienDamage = $this->enemySpaceShip->attack($this->homeSpaceShip);
        $this->homeSpaceShip->setHealth($this->homeSpaceShip->getHealth() - $alienDamage);
        $this->enemySpaceShip->setHealth($this->enemySpaceShip->getHealth() - $humanDamage);
        $humanHealth = $this->homeSpaceShip->getHealth();
        $alienHealth = $this->enemySpaceShip->getHealth();

        $this->writer->writeln(
            Messages::attack(
                $this->enemySpaceShip->name,
                $humanDamage,
                $alienHealth,
                $alienDamage,
                $humanHealth
            )
        );

        $this->winner($humanHealth, $alienHealth, $classname);
    }

    public function winner(int $humanHealth, int $alienHealth, $classname)
    {
        if ($humanHealth === 0) {
            $this->writer->writeln(Messages::die());
            return;
        }

        if ($alienHealth === 0 && $classname === ExecutorSpaceShip::class) {
            $this->writer->writeln(Messages::finalWin());
            $this->restart();
        }

        if ($alienHealth === 0) {
            $classname::setDestroyed();
            $this->writer->writeln(Messages::destroyed($this->enemySpaceShip->name));
        }
    }

    private function getConcreteShip()
    {
        $class = get_class($this->enemySpaceShip);

        return $class;
    }
    public function buy($type, $galaxyLabel)
    {
        if ($galaxyLabel !== 'home') {
            $this->writer->writeln('Buy only in Home Galaxy! 🔋 🔮');
            return;
        }
        $element = explode(' ', $type)[1];

        $this->galaxy->buy($this->homeSpaceShip, $element);
    }

    public function grab($galaxyLabel)
    {
        if (!isset($this->enemySpaceShip)) {
            $this->writer->writeln('Select alien galaxy!');
            return;
        }
        $enemySpaceShip = $this->getConcreteShip();
        if ($enemySpaceShip::getDestroyed()) {
            $this->homeSpaceShip->grab(
                $this->enemySpaceShip->getHold(),
                $this->enemySpaceShip->name
            );
        } else {
            $this->writer->writeln(Messages::errors('grab_undestroyed_spaceship'));
        }

        if ($galaxyLabel === 'home') {
            $this->writer->writeln('You can\'t grab any treasure!');
            return;
        }
    }

    private function restart()
    {
        $this->galaxy = new HomeGalaxy('home');
        $this->homeSpaceShip = new HomeSpaceShip('Traktor', 10, 5, 5, 100);
    }

    public function stats()
    {
        $data = [
            'strength' => $this->homeSpaceShip->getStrength(),
            'armor' => $this->homeSpaceShip->getArmor(),
            'luck' => $this->homeSpaceShip->getLuck(),
            'health' => $this->homeSpaceShip->getHealth(),
            'hold' => $this->homeSpaceShip->getHold(),
        ];

        $this->writer->writeln(Messages::stats($data));
    }
}
